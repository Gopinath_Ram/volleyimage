package com.example.gopi.volleyimage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

/**
 * Created by GOPI on 3/13/2017.
 */

public class MyAdapter extends BaseAdapter {
    private Context mContext;
    private List<Article> articleList;
    private LayoutInflater mLayoutInflator;
    private ImageLoader imageLoader;

    public MyAdapter(Context mContext, List<Article> articleList) {
        this.mContext = mContext;
        this.articleList = articleList;
        mLayoutInflator = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return articleList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null){
            view = mLayoutInflator.inflate(R.layout.item_layout,viewGroup,false);
        }
        if (imageLoader == null){
            imageLoader = VolleyImage.getInstance(mContext).getImageLoader();
        }
        TextView mAuthor = (TextView) view.findViewById(R.id.author);
        TextView mDesc = (TextView) view.findViewById(R.id.description);
        TextView mTitle = (TextView) view.findViewById(R.id.title);
        TextView mUrl = (TextView) view.findViewById(R.id.url);
        TextView mUrlToImage = (TextView) view.findViewById(R.id.urlToimage);
        TextView mPublish = (TextView) view.findViewById(R.id.publishedat);
        NetworkImageView networkImageView = (NetworkImageView)view.findViewById(R.id.networkImageview);
        networkImageView.setImageUrl(articleList.get(i).getUrlToImage(),imageLoader);
        mAuthor.setText(articleList.get(i).getAuthor());
        mDesc.setText(articleList.get(i).getDescription());
        mTitle.setText(articleList.get(i).getTitle());
        mUrl.setText(articleList.get(i).getUrl());
        mUrlToImage.setText(articleList.get(i).getUrlToImage());
        mPublish.setText(articleList.get(i).getPublishedAt());
        return view;
    }
}
