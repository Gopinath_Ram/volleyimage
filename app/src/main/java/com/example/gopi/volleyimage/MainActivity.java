package com.example.gopi.volleyimage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private String url =
            "https://newsapi.org/v1/articles?source=bbc-sport&sortBy=top&apiKey=2ef595b2741e4a579066eef4f638650a";
    private TextView mStatusTv,mSourceTv,mSortByTv;
    private ListView mListview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*mStatusTv = (TextView)findViewById(R.id.textView);
        mSourceTv = (TextView)findViewById(R.id.textView2);
        mSortByTv = (TextView)findViewById(R.id.textView3);*/
        mListview = (ListView)findViewById(R.id.listview);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Toast.makeText(getApplicationContext(),String.valueOf(response),Toast.LENGTH_LONG).show();
                    Gson gson = new Gson();
                    ArticleList articleList = gson.fromJson(response.toString(),ArticleList.class);
                    MyAdapter myAdapter = new MyAdapter(MainActivity.this,articleList.getArticles());
                    mListview.setAdapter(myAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        VolleyImage.getInstance(MainActivity.this).addToRequestQueue(jsonObjectRequest);
    }
}
